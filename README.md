# Image in mongodb data base

objectifs: 
- arreter de persister des fichiers / images dans git
- arreter les livraisons de la brique

fonctionnalités :
- [x] stocker des fichiers / images dans une base de donner mongo
- [ ] gestion du referenciel dans une base de donnée mysql
- [X] si une image / fichier n'existe pas dans le réferenciel faire un appelle retrofit au cdn 
  - [ ] et l'ajouter au referentielle
- [ ] export / import des fichiers / images d'un environement à un autre




Utils :
http://localhost:8080/photo/colorful-powder-explosion-in-all-directions-in-a-nice-composition-picture-id890147976?s=612x612
