package fr.louarn.mongodb.controler;

import fr.louarn.mongodb.model.Environment;
import fr.louarn.mongodb.model.Photo;
import fr.louarn.mongodb.service.PhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
public class PhotoRestController {

    /**
     * photoService.
     */
    private PhotoService photoService;

    /**
     * Constructeur avec paramètres.
     *
     * @param photoService photoService
     */
    @Autowired
    public PhotoRestController(PhotoService photoService) {
        this.photoService = photoService;
    }

    /**
     * getPhotoData.
     *
     * @param id id
     * @return image
     */
    @GetMapping(value = "/photo/{id}", produces = "image/*")
    public byte[] getPhotoData(@PathVariable String id) throws IOException {
        Photo photo = photoService.getPhoto(id);
        return photo.getImage().getData();
    }

    // TODO : Création d'un inport
    /**
     * getPhotoData.
     *
     * @param id          id
     * @param environment environment
     */
    @PostMapping(value = "/photo/{id}/export/{environment}")
    public void exportPhoto(@PathVariable String id,
                              @PathVariable Environment environment) throws IOException {
        photoService.exportImage(id, environment);
    }
}
