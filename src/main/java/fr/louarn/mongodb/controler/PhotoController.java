package fr.louarn.mongodb.controler;

import fr.louarn.mongodb.model.Photo;
import fr.louarn.mongodb.service.PhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Base64;

@Controller
public class PhotoController {

    /**
     * photoService.
     */
    private PhotoService photoService;

    /**
     * Controller avec paramètres.
     *
     * @param photoService photoService
     */
    @Autowired
    public PhotoController(PhotoService photoService) {
        this.photoService = photoService;
    }

    /**
     * getPhoto.
     *
     * @param id    id
     * @param model model
     * @return page html photos
     */
    @GetMapping("/photos/{id}")
    public String getPhoto(@PathVariable String id, Model model) throws IOException {
        Photo photo = photoService.getPhoto(id);
        model.addAttribute("title", photo.getTitle());
        model.addAttribute("image", Base64.getEncoder().encodeToString(photo.getImage().getData()));
        return "photos";
    }

    /**
     * uploadPhoto.
     *
     * @param model model
     * @return page html uploadPhoto
     */
    @GetMapping("/photos/upload")
    public String uploadPhoto(Model model) {
        model.addAttribute("message", "hello");
        return "uploadPhoto";
    }

    /**
     * addPhoto.
     *
     * @param title title
     * @param image image
     * @param model model
     * @return page html photos
     * @throws IOException
     */
    @PostMapping("/photos/add")
    public String addPhoto(@RequestParam("title") String title,
                           @RequestParam("image") MultipartFile image,
                           Model model) throws IOException {
        String id = photoService.addPhoto(title, image);
        return "redirect:/photos/" + id;
    }
}
