package fr.louarn.mongodb.dao.rest.impl;

import fr.louarn.mongodb.dao.rest.abst.AbstractDao;
import fr.louarn.mongodb.dao.rest.ressource.CdnResource;
import fr.louarn.mongodb.model.Photo;
import okhttp3.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import retrofit2.Response;
import retrofit2.Retrofit;

import java.io.IOException;

@Repository
public class CdnDaoImpl extends AbstractDao<CdnResource> {

    /**
     * Injection de la configuration Retrofit.
     */
    private Retrofit retrofit;

    /**
     * Constructeur avec paramètre.
     *
     * @param retrofit retrofit
     */
    @Autowired
    public CdnDaoImpl(Retrofit retrofit) {
        this.retrofit = retrofit;
    }

    /**
     * findImage.
     *
     * @param url url
     * @return image
     * @throws IOException
     */
    public ResponseBody findImage(String url) throws IOException {
        Response<ResponseBody> response = execute(getResource().findImage(url));
        return getOrElseThrow(response);
    }

    /**
     * findImage.
     *
     * @param url   url
     * @param photo photo
     * @throws IOException
     */
    public void exportImage(String url, Photo photo) throws IOException {
        CdnResource cdnResource = new Retrofit.Builder()
                .baseUrl(url)
                .build()
                .create(getClazz());
        Response<Void> response = execute(cdnResource.exportImage(photo));
        getOrElseThrow(response);
    }

    @Override
    protected Class<CdnResource> getClazz() {
        return CdnResource.class;
    }

    @Override
    protected Retrofit getRetrofit() {
        return retrofit;
    }

    @Override
    protected <R> R getOrElseThrow(Response<R> response) throws IOException {
        if (!response.isSuccessful() && response.errorBody() != null) {
            throw getErrorBody(response.errorBody(), IOException.class).orElseThrow(IOException::new);
        }
        return response.body();
    }
}

