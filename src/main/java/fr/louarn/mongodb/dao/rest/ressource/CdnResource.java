package fr.louarn.mongodb.dao.rest.ressource;

import fr.louarn.mongodb.model.Photo;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface CdnResource {

    /**
     * findImage.
     *
     * @param url url
     * @return image
     */
    @GET(value = "photos/{url}")
    Call<ResponseBody> findImage(@Path("url") String url);

    /**
     * exportImage.
     *
     * @param url url
     * @return image
     */
    @POST(value = "photos/export")
    Call<Void> exportImage(@Body Photo url);

}
