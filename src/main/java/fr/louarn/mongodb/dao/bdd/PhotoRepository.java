package fr.louarn.mongodb.dao.bdd;

import fr.louarn.mongodb.model.Photo;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PhotoRepository extends MongoRepository<Photo, String> {

}
