package fr.louarn.mongodb.dao.bdd;

import fr.louarn.mongodb.model.CdnApi;
import fr.louarn.mongodb.model.Environment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "cdn_api", path = "cdn_api")
public interface CdnApiRepository extends JpaRepository<CdnApi, String> {

    /**
     * findByEnvironment.
     *
     * @param environment environment
     * @return List CdnApi
     */
    List<CdnApi> findByEnvironment(Environment environment);
}
