package fr.louarn.mongodb.service;

import fr.louarn.mongodb.dao.bdd.CdnApiRepository;
import fr.louarn.mongodb.dao.bdd.PhotoRepository;
import fr.louarn.mongodb.dao.rest.impl.CdnDaoImpl;
import fr.louarn.mongodb.model.Environment;
import fr.louarn.mongodb.model.Photo;
import lombok.extern.slf4j.Slf4j;
import org.bson.BsonBinarySubType;
import org.bson.types.Binary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Optional;

@Slf4j
@Service
public class PhotoService {

    /**
     * photoRepo.
     */
    private PhotoRepository photoRepo;

    /**
     * cdnApiRepository.
     */
    private CdnApiRepository cdnApiRepository;

    /**
     * cdnDao.
     */
    private CdnDaoImpl cdnDao;

    /**
     * Constructeur avec paramètres.
     *
     * @param photoRepo        photoRepo
     * @param cdnDao           cdnDao
     * @param cdnApiRepository cdnApiRepository
     */
    @Autowired
    public PhotoService(PhotoRepository photoRepo,
                        CdnDaoImpl cdnDao,
                        CdnApiRepository cdnApiRepository) {
        this.photoRepo = photoRepo;
        this.cdnDao = cdnDao;
        this.cdnApiRepository = cdnApiRepository;
    }

    /**
     * getPhoto.
     *
     * @param id id
     * @return photo
     * @throws IOException
     */
    public Photo getPhoto(String id) throws IOException {

        Optional<Photo> photo = photoRepo.findById(id);

        if (photo.isPresent()) {
            return photo.get();
        } else {
            return Photo
                    .builder()
                    .id(id)
                    .image(new Binary(cdnDao.findImage(id).bytes()))
                    .build();
        }
    }

    /**
     * addPhoto.
     *
     * @param title title
     * @param file  file
     * @return id
     * @throws IOException
     */
    public String addPhoto(String title, MultipartFile file) throws IOException {
        Photo photo = Photo.builder().title(title).build();
        photo.setImage(new Binary(BsonBinarySubType.BINARY, file.getBytes()));
        photo = photoRepo.insert(photo);
        return photo.getId();
    }


    /**
     * exportImage.
     *
     * @param id          id
     * @param environment environment
     */
    public void exportImage(String id, Environment environment) throws IOException {
        Photo photo = getPhoto(id);
        cdnApiRepository.findByEnvironment(environment).forEach(cdnApi -> {
            try {
                cdnDao.exportImage(cdnApi.getUrl(), photo);
            } catch (IOException e) {
                log.error("", e);
            }
        });
    }
}
