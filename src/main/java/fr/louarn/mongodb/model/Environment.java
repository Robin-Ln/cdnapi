package fr.louarn.mongodb.model;

public enum Environment {
    /**
     * VAL.
     */
    VAL,
    /**
     * REC.
     */
    REC,
    /**
     * HML.
     */
    HML,
    /**
     * PROD.
     */
    PROD
}
