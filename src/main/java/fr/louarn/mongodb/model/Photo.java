package fr.louarn.mongodb.model;

import lombok.*;
import org.bson.types.Binary;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "photos")
public class Photo {

    /**
     * id.
     */
    @Id
    private String id;

    /**
     * title.
     */
    private String title;

    /**
     * images.
     */
    private Binary image;

}
