package fr.louarn.mongodb.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

@Getter
@Setter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "cdn_api")
public class CdnApi implements Serializable {

    /**
     * id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "sequence")
    private Integer id;

    /**
     * environment.
     */
    private Environment environment;

    /**
     * url.
     */
    private String url;
}
