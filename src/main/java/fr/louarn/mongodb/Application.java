package fr.louarn.mongodb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

    /**
     * Lancement de l'application.
     *
     * @param args arguments
     */
    public static void main(String... args) {
        SpringApplication.run(Application.class, args);
    }

}
