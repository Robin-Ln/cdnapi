package fr.louarn.mongodb.config.yaml;

import lombok.Getter;
import lombok.Setter;
import okhttp3.logging.HttpLoggingInterceptor;

@Getter
@Setter
public class ApiYaml {

    /**
     * Url de l'api.
     */
    private String basePath;

    /**
     * Niveau de log.
     */
    private HttpLoggingInterceptor.Level level;
}
