package fr.louarn.mongodb.config.yaml;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "cdn")
public class CdnYaml extends ApiYaml {

    /**
     * url.
     */
    private String url;
}
