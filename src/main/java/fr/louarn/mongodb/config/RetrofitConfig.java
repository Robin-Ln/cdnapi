package fr.louarn.mongodb.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.louarn.mongodb.config.yaml.CdnYaml;
import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

@Slf4j
@Configuration
public class RetrofitConfig {

    /**
     * cdnYaml.
     */
    private CdnYaml cdnYaml;

    /**
     * objectMapper.
     */
    private ObjectMapper objectMapper;

    /**
     * Constructeur avec paramètre.
     *
     * @param cdnYaml      cdnYaml
     * @param objectMapper objectMapper
     */
    @Autowired
    public RetrofitConfig(CdnYaml cdnYaml, ObjectMapper objectMapper) {
        this.cdnYaml = cdnYaml;
        this.objectMapper = objectMapper;
    }

    /**
     * retrofitBuilder.
     *
     * @param utl utl
     * @return builder
     */
    public Retrofit.Builder retrofitBuilder(String utl) {
        OkHttpClient httpClient = okHttpClientBuilder(cdnYaml.getLevel()).build();
        return new Retrofit.Builder()
                .baseUrl(utl)
                .addConverterFactory(JacksonConverterFactory.create(objectMapper))
                .client(httpClient);
    }

    /**
     * Configuration de Retrofit pour cdn.
     *
     * @return une configuration Retrofit
     */
    @Bean
    public Retrofit cdn() {
        OkHttpClient httpClient = okHttpClientBuilder(cdnYaml.getLevel()).build();
        return new Retrofit.Builder()
                .baseUrl(cdnYaml.getBasePath())
                .addConverterFactory(JacksonConverterFactory.create(objectMapper))
                .client(httpClient)
                .build();
    }

    /**
     * okHttpClientBuilder.
     *
     * @param level level
     * @return OkHttpClient Builder
     */
    private OkHttpClient.Builder okHttpClientBuilder(HttpLoggingInterceptor.Level level) {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor(log::info);
        httpLoggingInterceptor.level(level);
        return new OkHttpClient
                .Builder()
                .addInterceptor(httpLoggingInterceptor);
    }
}
